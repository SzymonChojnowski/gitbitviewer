package com.example.szymon.gitbitviewer;

import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private static final String GIT_URL = "https://api.github.com/repositories";
    private static final String BIT_URL = "https://api.bitbucket.org/2.0/repositories?fields=values.name,values.owner,values.description";
    private RepositoryAdapter mAdapter;
    private RecyclerView mRecyclerView;
    private boolean mBold;
    private List<Repository> repositories;
    private List<Repository> org_repositories;

    public static boolean isConnected(Context context) {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (!isConnected(this)) {
            createAlertDialog();
        } else {
            RequestQueue queue = Volley.newRequestQueue(this);
            repositories = new ArrayList<>();
            mRecyclerView = findViewById(R.id.recycler_view);
            mRecyclerView.setHasFixedSize(true);
            mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
            mRecyclerView.setItemAnimator(new DefaultItemAnimator());
            loadDataFromGit(queue);
            loadDataFromBit(queue);
            RequestQueue.RequestFinishedListener listener =
                    new RequestQueue.RequestFinishedListener() {
                        @Override
                        public void onRequestFinished(Request request) {
                            org_repositories = new ArrayList<>(repositories);
                            RepositoryAdapter mAdapter = new RepositoryAdapter(MainActivity.this, repositories);
                            mRecyclerView.setAdapter(mAdapter);
                        }
                    };
            queue.addRequestFinishedListener(listener);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.dot_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.sort:
                if (item.isChecked()) {
                    item.setChecked(false);
                    mBold = false;
                } else {
                    item.setChecked(true);
                    mBold = true;
                }
                sortRepos();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    protected void sortRepos() {
        if (mBold) {
            Collections.sort(repositories, Repository.BY_REPO_NAME_ALPHABETICAL);
            mAdapter = new RepositoryAdapter(MainActivity.this, repositories);
            mRecyclerView.setAdapter(mAdapter);
        } else if (!mBold) {
            mAdapter = new RepositoryAdapter(MainActivity.this, org_repositories);
            mRecyclerView.setAdapter(mAdapter);
        }
    }

    private void loadDataFromGit(RequestQueue queue) {
        StringRequest stringRequest = new StringRequest(Request.Method.GET, GIT_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONArray array = new JSONArray(response);
                            for (int i = 0; i < array.length(); i++) {
                                JSONObject item = array.getJSONObject(i);
                                repositories.add(new Repository(
                                        item.getString("name"),
                                        item.getJSONObject("owner").getString("login"),
                                        item.getJSONObject("owner").getString("avatar_url"),
                                        item.getString("description"),
                                        false
                                ));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                    }
                });
        queue.add(stringRequest);
    }

    private void loadDataFromBit(RequestQueue queue) {
        StringRequest stringRequest = new StringRequest(Request.Method.GET, BIT_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject result = new JSONObject(response);
                            JSONArray array = result.getJSONArray("values");
                            for (int i = 0; i < array.length(); i++) {
                                JSONObject item = array.getJSONObject(i);
                                repositories.add(new Repository(
                                        item.getString("name"),
                                        item.getJSONObject("owner").getString("display_name"),
                                        item.getJSONObject("owner").getJSONObject("links").getJSONObject("avatar").getString("href"),
                                        item.getString("description"),
                                        true
                                ));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                    }
                });
        queue.add(stringRequest);
    }

    public void createAlertDialog() {
        AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
        alertDialog.setTitle("No internet connection");
        alertDialog.setCancelable(false);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setMessage("You need to be online to see the data.");
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Ok",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        System.exit(0);
                    }
                });
        alertDialog.show();
    }
}
