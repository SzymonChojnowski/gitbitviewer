package com.example.szymon.gitbitviewer;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

public class DetailsActivity extends AppCompatActivity {

    TextView textRepositoryName, textDesc, textUser;
    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        String repositoryName = getIntent().getStringExtra("repositoryName");
        String username = getIntent().getStringExtra("username");
        String description = getIntent().getStringExtra("description");

        textRepositoryName = findViewById(R.id.textViewRepositoryName);
        textUser = findViewById(R.id.textViewUsername);
        textDesc = findViewById(R.id.textDesc);
        imageView = findViewById(R.id.imageView);

        textRepositoryName.setText(repositoryName);
        textUser.setText(username);
        textDesc.setText(description);
        Bitmap bitmap = getIntent().getParcelableExtra("bitmap");
        imageView.setImageBitmap(bitmap);


        getIntent().removeExtra("repositoryName");
        getIntent().removeExtra("username");
        getIntent().removeExtra("description");
        getIntent().removeExtra("avatarUrl");
    }
}
