package com.example.szymon.gitbitviewer;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

public class RepositoryAdapter extends RecyclerView.Adapter<RepositoryAdapter.ViewHolder> implements View.OnClickListener {

    private Context mContext;
    private List<Repository> mRepositories;

    public RepositoryAdapter(Context context, List<Repository> repositories) {
        this.mContext = context;
        this.mRepositories = repositories;
    }

    @Override
    public RepositoryAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list, parent, false);
        final ViewHolder viewHolder = new ViewHolder(v);
        viewHolder.itemView.setOnClickListener(this);
        viewHolder.itemView.setTag(viewHolder);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Repository item = mRepositories.get(position);

        if (!item.getmAvatarUrl().equals(null)) {
            Glide.with(mContext)
                    .load(item.getmAvatarUrl())
                    .into(holder.imageView);
        }
        holder.textViewRepositoryName.setText(item.getmRepositoryName());
        holder.textViewUsername.setText(item.getUsername());
        if (item.ismIsFromBitBucket()) {
            holder.bitBucketView.setVisibility(View.VISIBLE);
            holder.bitBucketView.setImageResource(R.drawable.bitbucket);
        } else {
            holder.bitBucketView.setImageResource(R.drawable.github);
        }
    }

    @Override
    public int getItemCount() {
        return mRepositories.size();
    }

    public Activity getActivity(Context context) {
        if (context == null) {
            return null;
        } else if (context instanceof ContextWrapper) {
            if (context instanceof Activity) {
                return (Activity) context;
            } else {
                return getActivity(((ContextWrapper) context).getBaseContext());
            }
        }
        return null;
    }

    @Override
    public void onClick(View view) {
        ViewHolder holder = (ViewHolder) view.getTag();
        int position = holder.getAdapterPosition();
        holder.imageView.setDrawingCacheEnabled(true);
        Bitmap b = holder.imageView.getDrawingCache();

        String repositoryName = mRepositories.get(position).getmRepositoryName();
        String username = mRepositories.get(position).getUsername();
        String description = mRepositories.get(position).getmDescription();
        Intent myIntent = new Intent(getActivity(mContext), DetailsActivity.class);
        myIntent.putExtra("bitmap", b);
        myIntent.putExtra("repositoryName", repositoryName);
        myIntent.putExtra("username", username);
        myIntent.putExtra("description", description);
        mContext.startActivity(myIntent);
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView textViewRepositoryName, textViewUsername;
        ImageView imageView, bitBucketView;
        CardView cardView;

        public ViewHolder(View itemView) {
            super(itemView);
            textViewRepositoryName = itemView.findViewById(R.id.textViewRepositoryName);
            textViewUsername = itemView.findViewById(R.id.textViewUsername);
            imageView = itemView.findViewById(R.id.imageView);
            bitBucketView = itemView.findViewById(R.id.bitBucketView);
            cardView = itemView.findViewById(R.id.cardView_id);
        }
    }
}
