package com.example.szymon.gitbitviewer;

import java.util.Comparator;

public class Repository {
    public static final Comparator<Repository> BY_REPO_NAME_ALPHABETICAL = new Comparator<Repository>() {
        @Override
        public int compare(Repository item1, Repository item2) {
            return item1.mRepositoryName.compareToIgnoreCase(item2.mRepositoryName);
        }
    };
    private String mRepositoryName;
    private String username;
    private String mAvatarUrl;
    private String mDescription;
    private boolean mIsFromBitBucket;

    public Repository(String repository, String username, String avatarUrl, String description, boolean isFromBitBucket) {
        this.mRepositoryName = repository;
        this.username = username;
        this.mAvatarUrl = avatarUrl;
        this.mDescription = description;
        this.mIsFromBitBucket = isFromBitBucket;
    }

    public String getmRepositoryName() {
        return mRepositoryName;
    }

    public String getUsername() {
        return username;
    }

    public String getmAvatarUrl() {
        return mAvatarUrl;
    }

    public String getmDescription() {
        return mDescription;
    }

    public boolean ismIsFromBitBucket() {
        return mIsFromBitBucket;
    }
}
